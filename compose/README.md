# Compose

`docker-compose` deployment of `cbor`. 

## Services

Services built as microservices architecture. there are three main services in the
system `gateway`, `cborer` and `auth`. `gateway` is the microservices api-gateway 
service(built with `golang`). The requests received to gateway will be redirected 
to `cborer` service(built with `scala`). `cborer` service will do actual cbor stream 
manipulating functions. `auth` service is the `JWT` based authentication/authorization 
services in the platfrom. It do user authentication/authorization and issue auth token. 
The auth tokens needs to be sent with HTTP request header as `Bearer`. These auth 
tokens will be verified by `gateway` service. Currently auth token verification not 
enabled in the platfrom. To enable disable auth token verification, there is a feature 
toggle `enableVerifyToken` in `gateway` service. The architecture of the platfrom 
shown in following figure. 

![Alt text](cbor.png?raw=true "Architecture")

`gateway` services exposes following REST endpoints to outside. These endpoints 
exposes as JSON based HTTP POST APIs.

```
http://localhost:7654/api/encode
http://localhost:7654/api/decode
```

`api/encode` consumes following JSON object. It set the `value` into the given 
`path` of cbor `stream` and return as a hex encoded cbor string. 

```
{
  "stream": <cbor stream as hex string>,
  "path": <path of cbor>,
  "value": <setting value>
}
```

`api/decode` consumes following JSON object. It extract the value associated in 
given `path` from cbor `stream` and return as a string. 

```
{
  "stream": <cbor stream as hex string>,
  "path": <path of cbor>
}
```

## Deploy services

Change `host.docker.local` field in `.env` file to local machines ip or add
a host entry to `/etc/hosts` file by overriding `host.docker.local` with local
machines ip. In mac-os `host.docker.local` will route to localhost, so nothing to
change no mac-os.

```
# example /etc/hosts file
10.4.1.104    host.docker.local
```

Start service in following order

```
docker-compose up -d gateway
docker-compose up -d cborer
```

Service logs can be viewed via docker

```
docker logs -f --tail 0 gateway
docker logs -f --tail 0 cborer
```

## Test services

Following is a sample cbor stream to test the APIs. It contains `map` type
data fields. Cbor streams can be generated from [here](http://cbor.me/)

```
A3646C616E67657363616C61636F70736A66756E6374696F6E616C66726174696E6705
{"lang": "scala", "ops": "functional", "rating": 5}
```

Encode API can test with following `curl` request. On successful encode, a hex
encoded cbor string will be returned.

```
# request
curl -XPOST "http://localhost:7654/api/encode" \
--header "Content-Type: application/json" \
--data '
{
  "stream": "A3646C616E67657363616C61636F70736A66756E6374696F6E616C66726174696E6705",
  "path": "lang",
  "value": "haskell"
}
'

# response
A3646C616E67676861736B656C6C636F70736A66756E6374696F6E616C66726174696E6705
```

Decode API can test with following `curl` request. On successful decode, the
value associated in given path will be returned. 

```
# request
curl -XPOST "http://localhost:7654/api/decode" \
--header "Content-Type: application/json" \
--data '
{
  "stream": "A3646C616E67657363616C61636F70736A66756E6374696F6E616C66726174696E6705",
  "path": "lang"
}
'

# response
scala
```
