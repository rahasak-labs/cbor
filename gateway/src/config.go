package main

import (
	"os"
)

type Config struct {
	serviceName string
	authRsaPub  string
	dotLogs     string
}

type FeatureToggle struct {
	enableVerifyToken string
}

type ApiConfig struct {
	encodeApi string
	decodeApi string
}

var config = Config{
	serviceName: getEnv("SERVICE_NAME", "gateway"),
	authRsaPub:  getEnv("AUTH_RSA_PUB", "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCgAqBFZxpTfXqAFB4oQI+vvO/MCblRzImOGIX3SuZaq959HHybfXwnSNrlO35SCQglA0tJw5HwnhrHdOMINWFGmSRa9wEhYv49HiMwlJkFR5rZIqQk/141SxFolyVBZPLMP/wOvYnZOEUygZ9lSexoKQqIhh+al4lToeDWEL2u/wIDAQAB"),
	dotLogs:     getEnv("DOT_LOGS", ".logs"),
}

var featureToggle = FeatureToggle{
	enableVerifyToken: getEnv("ENABLE_VERIFY_TOKEN", "no"),
}

var apiConfig = ApiConfig{
	encodeApi: getEnv("ENCODE_API", "http://dev.localhost:8761/api/encode"),
	decodeApi: getEnv("DECODE_API", "http://dev.localhost:8761/api/encode"),
}

func getEnv(key, fallback string) string {
	if value, ok := os.LookupEnv(key); ok {
		return value
	}

	return fallback
}
