package main

import (
	"github.com/gorilla/mux"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
)

func initApi() {
	// router
	r := mux.NewRouter()
	r.HandleFunc("/api/auth", apiAuth).Methods("POST")
	r.HandleFunc("/api/encode", apiEncode).Methods("POST")
	r.HandleFunc("/api/decode", apiDecode).Methods("POST")

	log.Printf("INFO: init api on port 7654")

	// start server
	err := http.ListenAndServe(":7654", r)
	if err != nil {
		log.Printf("ERROR: fail init http server, %s", err.Error)
		os.Exit(1)
	}
}

func apiAuth(w http.ResponseWriter, r *http.Request) {
	log.Printf("INFO: auth request, currently auth api not enabled")

	// TODO call jwt-auth service

	response(w, "auth api not enabled", 200)
}

func apiEncode(w http.ResponseWriter, r *http.Request) {
	// read body
	data, _ := ioutil.ReadAll(r.Body)
	defer r.Body.Close()

	log.Printf("INFO: request encode %s", string(data))

	// handle auth if feature toggle enabled
	if featureToggle.enableVerifyToken == "yes" {
		tokenStr := r.Header.Get("Bearer")
		err := verifyToken(tokenStr)
		if err != nil {
			// unauthorized
			log.Printf("ERROR: fail to verify auth token, %s", err.Error())
			response(w, "Fail to verify Bearer", 401)
			return
		}
	}

	// call cborer service to handle encode
	resp, status := post(data, apiConfig.encodeApi)

	// TODO handle additional functions with encoding

	response(w, resp, status)
}

func apiDecode(w http.ResponseWriter, r *http.Request) {
	// read body
	data, _ := ioutil.ReadAll(r.Body)
	defer r.Body.Close()

	log.Printf("INFO: request decode %s", string(data))

	// handle auth if feature toggle enabled
	if featureToggle.enableVerifyToken == "yes" {
		tokenStr := r.Header.Get("Bearer")
		err := verifyToken(tokenStr)
		if err != nil {
			// unauthorized
			log.Printf("ERROR: fail to verify auth token, %s", err.Error())
			response(w, "Fail to verify Bearer", 401)
			return
		}
	}

	// call cborer service to handle decode
	resp, status := post(data, apiConfig.decodeApi)

	// TODO handle additional functions with decoding

	response(w, resp, status)
}

func response(w http.ResponseWriter, resp string, status int) {
	w.WriteHeader(status)
	w.Header().Set("Content-Type", "application/json")
	io.WriteString(w, resp)
}
