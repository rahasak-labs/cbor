package main

import (
	"crypto"
	"crypto/rsa"
	"crypto/sha256"
	"crypto/x509"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"strings"
	"time"
)

type Token struct {
	Id        string `json:"id"`
	Roles     string `json:"roles"`
	Company   string `json:"company"`
	IssueTime int32  `json:"issueTime"`
	Ttl       int    `json:"ttl"`
	Digsig    string `json:"digsig"`
}

func verifyToken(tokenStr string) error {
	log.Printf("INFO: verify token %s", tokenStr)

	// decond
	tokenJson, err := base64.StdEncoding.DecodeString(tokenStr)
	if err != nil {
		log.Printf("ERROR: fail to base64 decode, %s", err.Error())
		return err
	}

	// unmarshal
	var token Token
	err = json.Unmarshal(tokenJson, &token)
	if err != nil {
		log.Printf("ERROR: fail to unmarshla json, %s", err.Error())
		return err
	}

	// get public key of auth
	// compose payload
	pubkey := getAuthRsaPub()
	payload := fmt.Sprintf("%s%s%s%d%d", token.Id, token.Roles, token.Company, token.IssueTime, token.Ttl)

	log.Printf("INFO: verify payload %s with signarue %s", payload, token.Digsig)

	// verify token
	// token digitally sign by auth service
	err = verifySignature(payload, token.Digsig, pubkey)
	if err != nil {
		log.Printf("ERROR: cannot verify token signarue")
		return err
	}

	// check expire time
	now := int32(time.Now().Unix())
	expire := token.IssueTime + int32(token.Ttl*60)
	log.Printf("INFO: now %d, ttl %d", now, expire)
	if now > expire {
		log.Printf("ERROR: expired auth token")
		return errors.New("Expired auth token")
	}

	return nil
}

func getAuthRsaPub() *rsa.PublicKey {
	// key is base64 encoded
	data, err := base64.StdEncoding.DecodeString(config.authRsaPub)
	if err != nil {
		log.Printf("ERROR: fail get authRsaPub, %s", err.Error())
		return nil
	}

	// get rsa public key
	pub, err := x509.ParsePKIXPublicKey(data)
	if err != nil {
		log.Printf("ERROR: fail get authRsaPub, %s", err.Error())
		return nil
	}
	switch pub := pub.(type) {
	case *rsa.PublicKey:
		return pub
	default:
		return nil
	}

	return nil
}

func verifySignature(payload string, signature64 string, key *rsa.PublicKey) error {
	// decode base64 encoded signature
	signature, err := base64.StdEncoding.DecodeString(signature64)
	if err != nil {
		log.Printf("ERROR: fail to base64 decode, %s", err.Error())
		return err
	}

	// remove unwated characters and get sha256 hash of the payload
	replacer := strings.NewReplacer("\n", "", "\r", "", " ", "")
	fPayload := strings.TrimSpace(replacer.Replace(payload))
	hashed := sha256.Sum256([]byte(fPayload))

	return rsa.VerifyPKCS1v15(key, crypto.SHA256, hashed[:], signature)
}
