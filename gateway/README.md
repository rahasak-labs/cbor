# Gateway

Microservice api gateway of `cbor` encoder/decoder platfrom

## Build

Build via `docker`

```
docker build erangaeb/cbor-gateway:0.1 .
```

## Run

Run via `docker-compose` [here](https://gitlab.com/rahasak-labs/cbor/tree/master/compose)
