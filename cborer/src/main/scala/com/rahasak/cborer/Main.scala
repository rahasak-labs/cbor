package com.rahasak.cborer

import akka.actor.ActorSystem
import com.rahasak.cborer.actor.HttpActor
import com.rahasak.cborer.actor.HttpActor.Serve
import com.rahasak.cborer.config.AppConf
import com.rahasak.cborer.util.LoggerFactory

object Main extends App with AppConf {

  // setup logging
  LoggerFactory.init()

  // serve http actor
  implicit val system = ActorSystem.create("cborer")
  system.actorOf(HttpActor.props(), name = "HttpActor") ! Serve

}

