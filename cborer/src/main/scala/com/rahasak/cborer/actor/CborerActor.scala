package com.rahasak.cborer.actor

import akka.actor.{Actor, Props}
import akka.stream.ActorMaterializer
import com.rahasak.cborer.actor.CborerActor.{Decode, Encode}
import com.rahasak.cborer.cbor.CborFactory
import com.rahasak.cborer.config.AppConf
import com.rahasak.cborer.protocol.StatusReply
import com.rahasak.cborer.util.AppLogger

import scala.util.{Failure, Success}

object CborerActor {

  case class Decode(stream: String, path: String)

  case class Encode(stream: String, path: String, value: String)

  def props()(implicit materializer: ActorMaterializer) = Props(new CborerActor)

}

class CborerActor()(implicit materializer: ActorMaterializer) extends Actor with AppConf with AppLogger {
  override def receive: Receive = {
    case decode: Decode =>
      logger.info(s"got decode message - $decode")

      CborFactory.decode(decode.stream, decode.path) match {
        case Success(value) =>
          sender ! value
        case Failure(e) =>
          sender ! StatusReply(400, s"fail to decode, ${e.getMessage}")
      }

      context.stop(self)
    case encode: Encode =>
      logger.info(s"got encode message - $encode")

      CborFactory.encode(encode.stream, encode.path, encode.value) match {
        case Success(stream) =>
          sender ! stream
        case Failure(e) =>
          sender ! StatusReply(400, s"fail to encode, ${e.getMessage}")
      }

      context.stop(self)
  }
}
