package com.rahasak.cborer.actor

import akka.actor.{Actor, ActorSystem, Props}
import akka.http.scaladsl.Http
import akka.http.scaladsl.model._
import akka.http.scaladsl.server.Directives._
import akka.pattern.ask
import akka.stream.{ActorMaterializer, ActorMaterializerSettings, Supervision}
import akka.util.Timeout
import com.rahasak.cborer.actor.CborerActor.{Decode, Encode}
import com.rahasak.cborer.actor.HttpActor.Serve
import com.rahasak.cborer.config.AppConf
import com.rahasak.cborer.protocol.StatusReply
import com.rahasak.cborer.util.AppLogger

import scala.concurrent.ExecutionContextExecutor
import scala.concurrent.duration._
import scala.util.{Failure, Success}

object HttpActor {

  case class Serve()

  def props()(implicit system: ActorSystem) = Props(new HttpActor)

}

class HttpActor()(implicit system: ActorSystem) extends Actor with AppLogger with AppConf {

  override def receive: Receive = {
    case Serve =>
      // supervision
      // meterializer for streams
      val decider: Supervision.Decider = { e =>
        logError(e)
        Supervision.Resume
      }
      implicit val materializer = ActorMaterializer(ActorMaterializerSettings(system).withSupervisionStrategy(decider))
      implicit val ec = system.dispatcher

      Http().bindAndHandle(route, "0.0.0.0", servicePort)
  }

  def route()(implicit materializer: ActorMaterializer, ec: ExecutionContextExecutor) = {
    implicit val timeout = Timeout(10.seconds)

    import com.rahasak.cborer.protocol.StatusReplyProtocol._

    pathPrefix("api") {
      path("encode") {
        post {
          implicit val encodeFormat = jsonFormat3(Encode)
          entity(as[Encode]) {
            encode =>
              val f = context.actorOf(CborerActor.props()) ? encode
              onComplete(f) {
                case Success(stream: String) =>
                  logger.info(s"encode done $stream")
                  complete(StatusCodes.OK -> stream)
                case Success(status: StatusReply) =>
                  logger.info(s"encode complete with status, $status")
                  complete(StatusCodes.BadRequest -> status)
                case Failure(e) =>
                  logError(e)
                  complete(StatusCodes.BadRequest -> StatusReply(400, "error"))
                case _ =>
                  logger.info("fail encode")
                  complete(StatusCodes.BadRequest -> StatusReply(400, "error"))
              }
          }
        }
      } ~ path("decode") {
        post {
          implicit val decodeFormat = jsonFormat2(Decode)
          entity(as[Decode]) {
            decode =>
              val f = context.actorOf(CborerActor.props()) ? decode
              onComplete(f) {
                case Success(value: String) =>
                  logger.info(s"decode done $value")
                  complete(StatusCodes.OK -> value)
                case Success(status: StatusReply) =>
                  logger.info(s"decode complete with status, $status")
                  complete(StatusCodes.BadRequest -> status)
                case Failure(e) =>
                  logError(e)
                  complete(StatusCodes.BadRequest -> StatusReply(400, "error"))
                case _ =>
                  logger.info("fail decode")
                  complete(StatusCodes.BadRequest -> StatusReply(400, "error"))
              }
          }
        }
      }
    }
  }
}
