package com.rahasak.cborer.cbor

import java.io._
import java.math.BigInteger

import com.rahasak.cborer.util.AppLogger
import com.upokecenter.cbor.{CBORObject, CBORType}

import scala.util.{Failure, Success, Try}

object CborFactory extends AppLogger {

  def decode(stream: String, path: String): Try[String] = {
    Try {
      val bs = new BigInteger(stream, 16).toByteArray
      val bais = new ByteArrayInputStream(bs)

      while (bais.available() != 0) {
        val cbor = CBORObject.Read(bais)
        if (cbor.getType == CBORType.Map) {
          logger.info(s"cbor object $cbor")

          val value = cbor.get(path)
          if (value != null) {
            logger.info(s"found value $value on path $path")
            return Success(value.toString)
          } else {
            logger.error(s"given path '$path' not found in cbor stream")
          }
        }
      }

      throw new Exception(s"given path '$path' not found in cbor stream")
    } recoverWith {
      case e =>
        logError(e)
        Failure(e)
    }
  }

  def encode(stream: String, path: String, value: String): Try[String] = {
    Try {
      val bs = new BigInteger(stream, 16).toByteArray
      val bais = new ByteArrayInputStream(bs)

      while (bais.available() != 0) {
        val cbor = CBORObject.Read(bais)
        if (cbor.getType == CBORType.Map) {
          logger.info(s"cbor object $cbor")

          if (cbor.get(path) != null) {
            cbor.set(path, CBORObject.FromObject(value))
            logger.info(s"encoded cbor object $cbor")

            val encStrm = cbor.EncodeToBytes().map("%02X" format _).mkString
            logger.info(s"hex encoded stream $encStrm")

            return Success(encStrm)
          }
        }
      }

      throw new Exception(s"given path '$path' not found in cbor stream")
    } recoverWith {
      case e =>
        logError(e)
        Failure(e)
    }
  }

}

//object M extends App {
//  println(CborFactory.encode("A36368616B666572616E676163646F74636F70736373636101", "dot", "lambada"))
//  println(CborFactory.decode("A36368616B666572616E676163646F74636F70736373636101", "dot"))
//}
