package com.rahasak.cborer.protocol

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import spray.json._

case class StatusReply(code: Int, msg: String)

object StatusReplyProtocol extends SprayJsonSupport with DefaultJsonProtocol {
  implicit val statusReplyFormat = jsonFormat2(StatusReply)
}
