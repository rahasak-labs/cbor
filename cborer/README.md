# Cborer

`cbor` object encoder/decoder service

## Build

Build with `docker`

```
sbt assembly
docker build erangaeb/cborer:0.1 .
```

## Run

Run via `docker-compose` [here](https://gitlab.com/rahasak-labs/cbor/tree/master/compose)
